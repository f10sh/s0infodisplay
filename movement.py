#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time
from subprocess import call
GPIO.setmode(GPIO.BCM)

GPIO.setup(4, GPIO.IN)
#GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
print("works so far")

pir = 4
previous_state = 0
current_state = 0

try:
    while True:
        time.sleep(0.1)
        current_state = GPIO.input(pir)
        if current_state == 1 and previous_state == 0:
            print("GPIO pin %s is %s, turning screen on" % (pir, current_state))
            call("echo on 0 | cec-client -s -d 1", shell=True)
            previous_state = 1
        elif current_state == 0 and previous_state == 1:
            print("GPIO pin %s is %s, turning screen off" % (pir, current_state))
            call("echo standby 0 | cec-client -s -d 1", shell=True)
            previous_state = 0
        time.sleep(0.01)

except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
