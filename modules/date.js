function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function date() {
    var d = new Date();
    return DOW[d.getDay()]+', '+
        pad(d.getDate(),2)+'.'+pad(d.getMonth()+1,2)+'.'+d.getFullYear()+' '+
        pad(d.getHours(),2)+':'+pad(d.getMinutes(),2)+':'+pad(d.getSeconds(),2);
}


module.exports = function (io) {
    setInterval(function () {
        io.emit('date', '<div align="right"><h2>'+date()+'&nbsp;</h2></div>');
    }, 1000);
}
