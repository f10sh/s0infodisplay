var httpreq = require('httpreq');
var Mustache = require('mustache');

var URL = 'https://stratum0.org/status/status.json';
var TEMPLATES = {};
require('fs').readFile('modules/brand/template.mustache', 'utf-8', function (err, data) {
    TEMPLATES.template = data;
});
require('fs').readFile('modules/brand/status.mustache', 'utf-8', function (err, data) {
    TEMPLATES.status = data;
});

var status = {};

function renderStatus(sock, everything) {
    var sendInner = function () {
        status.random = ''+Math.random();
        sock.emit('brand.status', Mustache.render(TEMPLATES.status, status));
    }
    if (everything) {
        sock.emit('brand', Mustache.render(TEMPLATES.template, {}));
        setTimeout(sendInner, 3000);
    } else {
        sendInner();
    }
}

function fetchStatus(cb) {
    httpreq.get(URL, function (err, res) {
        var state = JSON.parse(res.body).state;
        cb(state);
    });
}

module.exports = function (io) {
    var firstTime = true;
    function update() {
        fetchStatus(function (state) {
            if (status.lastchange != state.lastchange) {
                var d = new Date(state.lastchange*1000);
                state.since = DOW[d.getDay()]+', '+pad(d.getHours(),2)+':'
                    +pad(d.getMinutes(),2);
                status = state;
                renderStatus(io);
            }
            if (firstTime && status) {
                io.on('connection', function (sock) {
                    renderStatus(sock, true);
                });
                renderStatus(io, true);
                firstTime = false;
            } else {
                renderStatus(io, false);
            }
        });
    }
    setInterval(update, 2*60*1000); //every 2 minutes
    update();
}
