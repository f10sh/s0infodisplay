var irc = require('irc');

var CHANNEL = '#stratum0';

var PW = 'Fagee9ie'

module.exports = function (io) {
    var client = new irc.Client('bouncer.ksal.de', 'infodisplay', {
        channels: [CHANNEL],
        port: 28921,
        secure: true,
        selfSigned: true,
        userName: 'infodisplay/Freenode',
        password: PW
    });
    var content = [];
    client.addListener('message', function (from, to, message) {
        if (to != CHANNEL || from === undefined) {return;}
        message = message.replace(/</g,'&lt;').replace(/>/g,'&gt;');
        content.push('<p><span>'+from+'</span> '+message+'</p>');
        if (content.length > 25) {
            content.shift();
        }
        io.emit('irc.inner', content.join(''));
    });
    io.on('connect', function (sock) {
        sock.emit('irc', '<h3>&nbsp;IRC #stratum0</h3><div class="chat" data-infodisplay-outlet="inner"></div>');
        setTimeout(function () {
            sock.emit('irc.inner', content.join(''));
        }, 3000);
    });
}
