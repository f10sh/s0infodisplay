var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8000);

app.use(express.static(__dirname + '/public'));

var instanceClients = [];
io.on('connection', function (socket) {
    socket.on('ident', function (client) {
        if (instanceClients.indexOf(client) === -1) {
            instanceClients.push(client);
            socket.emit('meta', 'reload');
        }
    })
});

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

// for reasons
DOW = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
pad = function (n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

var normalizedPath = require("path").join(__dirname, "modules");
require("fs").readdirSync(normalizedPath).forEach(function(file) {
    if (endsWith(file,'.js')) {
        try {
            require("./modules/" + file)(io);
            console.log(file+" loaded");
        } catch (e) {
            console.log("Error loading "+file);
            console.log(e);
        }
    }
});
